<?php
    require ('animal.php');
    require ('frog.php');
    require ('ape.php');

    $sheep = new Animal("shaun");

    echo "Name : $sheep->name <br>"; // "shaun"
    echo "Legs : $sheep->legs <br>"; // 4
    echo "Cool_Blooded : $sheep->cold_blooded <br><br>"; // "no"

    $kodok = new Frog("buduk");
    
    echo "Name : $kodok->name <br>"; 
    echo "Legs : $kodok->legs <br>"; 
    echo "Cool_Blooded : $kodok->cold_blooded <br>";
    echo "Jump : $kodok->jump <br><br>";
    
    $sungokong = new Ape("kera sakti");
    
    echo "Name : $sungokong->name <br>"; 
    echo "Legs : $sungokong->legs <br>"; 
    echo "Cool_Blooded : $sungokong->cold_blooded <br>";
    echo "Yell : $sungokong->yell <br>";
    ?>